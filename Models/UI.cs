using System.Text.RegularExpressions;
using System;
using System.Linq;
using System.Collections.Generic;
namespace Core;

public class UI
{

    static string[] key_words = { "DCL", "DECLARE", "BIN", "FIXED", "FLOAT", "CHAR" };


    public static GUI_Report GetGuiReport(Report r)
    {
        string error_report = "";
        string ids_report = "";
        string numbers_report = "";
        string semantic_warnings = "";
        Console.WriteLine($"Отладка {r.Pointer}");


        if (r.Error.HasValue)
        {
            List<string> lines = new List<string>();
            var err = r.Error.Value;

            string error_line = "";

            error_line += r.InputLine.Substring(0, r.Pointer);
            error_line += $"<{r.InputLine[r.Pointer]}>";
            error_line += r.InputLine.Substring(r.Pointer + 1, (r.InputLine.Length - 1) - r.Pointer);


            lines.Add(error_line);
            lines.Add($"Состояние: {r.StatesLog.Last()}");

            error_report = string.Join('\n', lines.ToArray());

        }
        else
        {
            error_report = "Строка соответсвует грамматике.\n";
            ids_report = GetIDReport(r.InputLine);
            numbers_report = GetNumReport(r.InputLine);
        }

        return new GUI_Report()
        {
            ErrorReport = error_report + semantic_warnings,
            NumbersReport = numbers_report,
            IDReport = ids_report,
            IsSuccess = r.Error.HasValue
        };
    }

    private static string GetNumReport(string ex)
    {
        string report = "";
        var items = ex.Split(',', ' ', '(', ')', ':');

        List<int> nums = new List<int>();

        foreach (string i in items)
        {
            int num;
            if (int.TryParse(i, out num))
            {
                report += $"{num} ";
                if ((num > 32767)||(num<(-32768)))
                {
                    report += "Выходит за пределы допустимых значений";
                }
                report += "\n";
            }
        }
        return report;
    }
    private static string GetIDReport(string ex)
    {

        ex = ex.Replace("(", " ( ");
        ex = ex.Replace(")", " ) ");
        ex = ex.Replace("  ", " ");
        ex = ex.Replace("DECLARE", "");
        ex = ex.Replace("DCL", "");


        var items = ex.Split(' ', ',', ';', '-');

        string[] keywords = { "BIN", "FIXED", "FLOAT", "CHAR" };
        int brackets_level = 0;
        List<string> IDS = new List<string>();
        List<Group> groups = new List<Group>();
        for (int i = 0; i < items.Count(); i++)
        {
            if (!string.IsNullOrWhiteSpace(items[i]))
            {
                Console.WriteLine($"Item: {items[i]} BL:{brackets_level} Stuff: {string.Join(" ,", IDS)}");
                if (items[i] == "(")
                {

                    brackets_level++;
                    Console.WriteLine("левелап");
                }
                else if (items[i] == ")")
                {
                    brackets_level--;
                    Console.WriteLine("левелдаун");
                }
                else if (brackets_level > 1)
                {
                    Console.WriteLine($"скип айтем {items[i]}");
                    continue;
                }
                else
                {
                    if (!keywords.Contains(items[i]))
                    {
                        IDS.Add(items[i]);
                    }
                    else if (keywords.Contains(items[i]))
                    {


                        int size = 0;
                        if (items[i] == "BIN")
                            size = 2;
                        else if (items[i] == "FLOAT")
                            size = 4;
                        else if (items[i] == "CHAR")
                        {
                            int.TryParse(items[i + 2], out size);
                            i += 4;
                        }
                        Group g = new Group()
                        {
                            Size = size,
                            Items = IDS.ToList(),
                            InBrackets = items[i - 1] == ")"
                        };
                        groups.Add(g);
                        IDS.Clear();
                        if (items[i] == "BIN") i++;
                    }
                }
            }

        }


        string report = "";

        foreach (var g in groups)
        {
            foreach (var i in g.Items)
            {
                if(Char.IsLetter(i[0]) || i[0]=='_')
									report += $"{i} - {g.Size} байт(а)\n";
            }
        }

        return report;

    }



    public struct Group
    {
        public bool InBrackets { get; init; }
        public int Size { get; init; }
        public List<string> Items { get; init; }

    }



    public struct GUI_Report
    {
        public string ErrorReport { get; init; }
        public string NumbersReport { get; init; }
        public string IDReport { get; init; }
        public bool IsSuccess { get; init; }
    }

}
