
using System;
using System.Collections.Generic;

namespace Core;

public static class Parser
{

    public static Report GetReport(string line)
    {
        Error? error = null;
        List<State> StateLog = new List<State>();
        int pointer = -1;
        char current_char;


        State current_state = State.S;
        bool Stop = false;

        while ((pointer < line.Length - 1) && (current_state != State.F) && !Stop)
        {
            pointer++;
            StateLog.Add(current_state);
            current_char = line[pointer];
            switch (current_state)
            {
                case State.S:
                    if (current_char == ' ') { current_state = State.S; }
                    else if (current_char == 'D') { current_state = State.A; }
                    else { Stop = true; }
                    break;
                case State.A:
                    if (current_char == 'C') { current_state = State.A1; }
                    else if (current_char == 'E') { current_state = State.B1; }
                    else { Stop = true; }
                    break;
                case State.A1:
                    if (current_char == 'L') { current_state = State.A2; }
                    else { Stop = true; }
                    break;
                case State.A2:
                    if (current_char == ' ') { current_state = State.B; }
                    else { Stop = true; }
                    break;
                case State.B1:
                    if (current_char == 'C') { current_state = State.B2; }
                    else { Stop = true; }
                    break;
                case State.B2:
                    if (current_char == 'L') { current_state = State.B3; }
                    else { Stop = true; }
                    break;
                case State.B3:
                    if (current_char == 'A') { current_state = State.B4; }
                    else { Stop = true; }
                    break;
                case State.B4:
                    if (current_char == 'R') { current_state = State.B5; }
                    else { Stop = true; }
                    break;
                case State.B5:
                    if (current_char == 'E') { current_state = State.B6; }
                    else { Stop = true; }
                    break;
                case State.B6:
                    if (current_char == ' ') { current_state = State.B; }
                    else { Stop = true; }
                    break;
                case State.B:
                    if (current_char == ' ') { current_state = State.B; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C1; }
                    else if (current_char == '_') { current_state = State.C1; }
                    else if (current_char == '(') { current_state = State.D1; }
                    else { Stop = true; }
                    break;
                case State.C1:
                    if (Char.IsLetter(current_char)) { current_state = State.C1; }
                    else if (current_char == '_') { current_state = State.C1; }
                    else if (Char.IsDigit(current_char)) { current_state = State.C1; }
                    else if (current_char == '(') { current_state = State.C2; }
                    else if (current_char == ',') { current_state = State.B; }
                    else if (current_char == ';') { current_state = State.F; }
                    else if (current_char == ' ') { current_state = State.C10; }
                    else { Stop = true; }
                    break;
                case State.C2:
                    if (current_char == '-') { current_state = State.C3; }
                    else if (current_char == '0') { current_state = State.C13; }
                    else if (current_char == '1') { current_state = State.C4; }
                    else if (current_char == '2') { current_state = State.C4; }
                    else if (current_char == '3') { current_state = State.C4; }
                    else if (current_char == '4') { current_state = State.C4; }
                    else if (current_char == '5') { current_state = State.C4; }
                    else if (current_char == '6') { current_state = State.C4; }
                    else if (current_char == '7') { current_state = State.C4; }
                    else if (current_char == '8') { current_state = State.C4; }
                    else if (current_char == '9') { current_state = State.C4; }
                    else if (current_char == ' ') { current_state = State.C2; }
                    else { Stop = true; }
                    break;
                case State.C3:
                    if (current_char == '1') { current_state = State.C4; }
                    else if (current_char == '2') { current_state = State.C4; }
                    else if (current_char == '3') { current_state = State.C4; }
                    else if (current_char == '4') { current_state = State.C4; }
                    else if (current_char == '5') { current_state = State.C4; }
                    else if (current_char == '6') { current_state = State.C4; }
                    else if (current_char == '7') { current_state = State.C4; }
                    else if (current_char == '8') { current_state = State.C4; }
                    else if (current_char == '9') { current_state = State.C4; }
                    else { Stop = true; }
                    break;
                case State.C13:
                    if (current_char == ',') { current_state = State.C2; }
                    else if (current_char == ' ') { current_state = State.C13; }
                    else if (current_char == ':') { current_state = State.C5; }
                    else if (current_char == ')') { current_state = State.C; }
                    else { Stop = true; }
                    break;
                case State.C5:
                    if (current_char == '-') { current_state = State.C6; }
                    else if (current_char == '0') { current_state = State.C15; }
                    else if (current_char == '1') { current_state = State.C7; }
                    else if (current_char == '2') { current_state = State.C7; }
                    else if (current_char == '3') { current_state = State.C7; }
                    else if (current_char == '4') { current_state = State.C7; }
                    else if (current_char == '5') { current_state = State.C7; }
                    else if (current_char == '6') { current_state = State.C7; }
                    else if (current_char == '7') { current_state = State.C7; }
                    else if (current_char == '8') { current_state = State.C7; }
                    else if (current_char == '9') { current_state = State.C7; }
                    else if (current_char == ' ') { current_state = State.C5; }
                    else { Stop = true; }
                    break;
                case State.C:
                    if (current_char == ',') { current_state = State.B; }
                    else if (current_char == ';') { current_state = State.F; }
                    else if (current_char == ' ') { current_state = State.CR; }
                    else { Stop = true; }
                    break;
                case State.C4:
                    if (Char.IsDigit(current_char)) { current_state = State.C4; }
                    else if (current_char == ',') { current_state = State.C2; }
                    else if (current_char == ':') { current_state = State.C5; }
                    else if (current_char == ')') { current_state = State.C; }
                    else if (current_char == ' ') { current_state = State.C13; }
                    else { Stop = true; }
                    break;
                case State.C6:
                    if (current_char == '1') { current_state = State.C7; }
                    else if (current_char == '2') { current_state = State.C7; }
                    else if (current_char == '3') { current_state = State.C7; }
                    else if (current_char == '4') { current_state = State.C7; }
                    else if (current_char == '5') { current_state = State.C7; }
                    else if (current_char == '6') { current_state = State.C7; }
                    else if (current_char == '7') { current_state = State.C7; }
                    else if (current_char == '8') { current_state = State.C7; }
                    else if (current_char == '9') { current_state = State.C7; }
                    else { Stop = true; }
                    break;
                case State.C15:
                    if (current_char == ' ') { current_state = State.C15; }
                    else if (current_char == ')') { current_state = State.C; }
                    else if (current_char == ',') { current_state = State.C2; }
                    else { Stop = true; }
                    break;
                case State.C7:
                    if (Char.IsDigit(current_char)) { current_state = State.C7; }
                    else if (current_char == ',') { current_state = State.C2; }
                    else if (current_char == ')') { current_state = State.C; }
                    else if (current_char == ' ') { current_state = State.C15; }
                    else { Stop = true; }
                    break;
                case State.D1:
                    if (Char.IsLetter(current_char)) { current_state = State.D2; }
                    else if (current_char == '_') { current_state = State.D2; }
                    else if (current_char == ' ') { current_state = State.D1; }
                    else { Stop = true; }
                    break;
                case State.D2:
                    if (Char.IsLetter(current_char)) { current_state = State.D2; }
                    else if (current_char == '_') { current_state = State.D2; }
                    else if (Char.IsDigit(current_char)) { current_state = State.D2; }
                    else if (current_char == ',') { current_state = State.D1; }
                    else if (current_char == '(') { current_state = State.D3; }
                    else if (current_char == ')') { current_state = State.R; }
                    else if (current_char == ' ') { current_state = State.D10; }
                    else { Stop = true; }
                    break;
                case State.D3:
                    if (current_char == '-') { current_state = State.D4; }
                    else if (current_char == '1') { current_state = State.D5; }
                    else if (current_char == '2') { current_state = State.D5; }
                    else if (current_char == '3') { current_state = State.D5; }
                    else if (current_char == '4') { current_state = State.D5; }
                    else if (current_char == '5') { current_state = State.D5; }
                    else if (current_char == '6') { current_state = State.D5; }
                    else if (current_char == '7') { current_state = State.D5; }
                    else if (current_char == '8') { current_state = State.D5; }
                    else if (current_char == '9') { current_state = State.D5; }
                    else if (current_char == '0') { current_state = State.D15; }
                    else if (current_char == ' ') { current_state = State.D3; }
                    else { Stop = true; }
                    break;
                case State.D4:
                    if (current_char == '1') { current_state = State.D5; }
                    else if (current_char == '2') { current_state = State.D5; }
                    else if (current_char == '3') { current_state = State.D5; }
                    else if (current_char == '4') { current_state = State.D5; }
                    else if (current_char == '5') { current_state = State.D5; }
                    else if (current_char == '6') { current_state = State.D5; }
                    else if (current_char == '7') { current_state = State.D5; }
                    else if (current_char == '8') { current_state = State.D5; }
                    else if (current_char == '9') { current_state = State.D5; }
                    else { Stop = true; }
                    break;
                case State.D5:
                    if (current_char == ' ') { current_state = State.D15; }
                    else if (Char.IsDigit(current_char)) { current_state = State.D5; }
                    else if (current_char == ',') { current_state = State.D3; }
                    else if (current_char == ':') { current_state = State.D6; }
                    else if (current_char == ')') { current_state = State.D9; }
                    else { Stop = true; }
                    break;
                case State.D15:
                    if (current_char == ' ') { current_state = State.D15; }
                    else if (current_char == ':') { current_state = State.D6; }
                    else if (current_char == ')') { current_state = State.D9; }
                    else if (current_char == ',') { current_state = State.D3; }
                    else { Stop = true; }
                    break;
                case State.D6:
                    if (current_char == '-') { current_state = State.D7; }
                    else if (current_char == '0') { current_state = State.D13; }
                    else if (current_char == '1') { current_state = State.D8; }
                    else if (current_char == '2') { current_state = State.D8; }
                    else if (current_char == '3') { current_state = State.D8; }
                    else if (current_char == '4') { current_state = State.D8; }
                    else if (current_char == '5') { current_state = State.D8; }
                    else if (current_char == '6') { current_state = State.D8; }
                    else if (current_char == '7') { current_state = State.D8; }
                    else if (current_char == '8') { current_state = State.D8; }
                    else if (current_char == '9') { current_state = State.D8; }
                    else if (current_char == ' ') { current_state = State.D6; }
                    else { Stop = true; }
                    break;
                case State.D9:
                    if (current_char == ',') { current_state = State.D1; }
                    else if (current_char == ')') { current_state = State.R; }
                    else if (current_char == ' ') { current_state = State.D9; }
                    else { Stop = true; }
                    break;
                case State.D7:
                    if (current_char == '1') { current_state = State.D8; }
                    else if (current_char == '2') { current_state = State.D8; }
                    else if (current_char == '3') { current_state = State.D8; }
                    else if (current_char == '4') { current_state = State.D8; }
                    else if (current_char == '5') { current_state = State.D8; }
                    else if (current_char == '6') { current_state = State.D8; }
                    else if (current_char == '7') { current_state = State.D8; }
                    else if (current_char == '8') { current_state = State.D8; }
                    else if (current_char == '9') { current_state = State.D8; }
                    else { Stop = true; }
                    break;
                case State.D13:
                    if (current_char == ' ') { current_state = State.D13; }
                    else if (current_char == ')') { current_state = State.D9; }
                    else if (current_char == ',') { current_state = State.D3; }
                    else { Stop = true; }
                    break;
                case State.D8:
                    if (Char.IsDigit(current_char)) { current_state = State.D8; }
                    else if (current_char == ',') { current_state = State.D3; }
                    else if (current_char == ' ') { current_state = State.D13; }
                    else if (current_char == ')') { current_state = State.D9; }
                    else { Stop = true; }
                    break;
                case State.R:
                    if (current_char == ' ') { current_state = State.R1; }
                    else { Stop = true; }
                    break;
                case State.C10:
                    if (current_char == 'C') { current_state = State.Y1; }
                    else if (current_char == 'B') { current_state = State.R2; }
                    else if (current_char == 'F') { current_state = State.T1; }
                    else if (current_char == ' ') { current_state = State.C10; }
                    else if (current_char == ',') { current_state = State.B; }
                    else if (current_char == '(') { current_state = State.C2; }
                    else if (current_char == ';') { current_state = State.F; }
                    else { Stop = true; }
                    break;
                case State.Y1:
                    if (current_char == 'H') { current_state = State.Y2; }
                    else { Stop = true; }
                    break;
                case State.R2:
                    if (current_char == 'I') { current_state = State.R3; }
                    else { Stop = true; }
                    break;
                case State.T1:
                    if (current_char == 'L') { current_state = State.T2; }
                    else { Stop = true; }
                    break;
                case State.R1:
                    if (current_char == ' ') { current_state = State.R1; }
                    else if (current_char == 'B') { current_state = State.R2; }
                    else if (current_char == 'F') { current_state = State.T1; }
                    else if (current_char == 'C') { current_state = State.Y1; }
                    else { Stop = true; }
                    break;
                case State.R3:
                    if (current_char == 'N') { current_state = State.R4; }
                    else { Stop = true; }
                    break;
                case State.R4:
                    if (current_char == ' ') { current_state = State.R5; }
                    else { Stop = true; }
                    break;
                case State.R5:
                    if (current_char == ' ') { current_state = State.R5; }
                    else if (current_char == 'F') { current_state = State.R6; }
                    else { Stop = true; }
                    break;
                case State.R6:
                    if (current_char == 'I') { current_state = State.R7; }
                    else { Stop = true; }
                    break;
                case State.R7:
                    if (current_char == 'X') { current_state = State.R8; }
                    else { Stop = true; }
                    break;
                case State.R8:
                    if (current_char == 'E') { current_state = State.R9; }
                    else { Stop = true; }
                    break;
                case State.R9:
                    if (current_char == 'D') { current_state = State.G4; }
                    else { Stop = true; }
                    break;
                case State.T2:
                    if (current_char == 'O') { current_state = State.T3; }
                    else { Stop = true; }
                    break;
                case State.T3:
                    if (current_char == 'A') { current_state = State.T4; }
                    else { Stop = true; }
                    break;
                case State.T4:
                    if (current_char == 'T') { current_state = State.G4; }
                    else { Stop = true; }
                    break;
                case State.Y2:
                    if (current_char == 'A') { current_state = State.Y3; }
                    else { Stop = true; }
                    break;
                case State.Y3:
                    if (current_char == 'R') { current_state = State.T; }
                    else { Stop = true; }
                    break;
                case State.T:
                    if (current_char == ' ') { current_state = State.T; }
                    else if (current_char == '(') { current_state = State.G1; }
                    else { Stop = true; }
                    break;
                case State.G1:
                    if (current_char == '-') { current_state = State.G2; }
                    else if (current_char == '1') { current_state = State.G3; }
                    else if (current_char == '2') { current_state = State.G3; }
                    else if (current_char == '3') { current_state = State.G3; }
                    else if (current_char == '4') { current_state = State.G3; }
                    else if (current_char == '5') { current_state = State.G3; }
                    else if (current_char == '6') { current_state = State.G3; }
                    else if (current_char == '7') { current_state = State.G3; }
                    else if (current_char == '8') { current_state = State.G3; }
                    else if (current_char == '9') { current_state = State.G3; }
                    else if (current_char == '0') { current_state = State.G5; }
                    else if (current_char == ' ') { current_state = State.G1; }
                    else { Stop = true; }
                    break;
                case State.G2:
                    if (current_char == '1') { current_state = State.G3; }
                    else if (current_char == '2') { current_state = State.G3; }
                    else if (current_char == '3') { current_state = State.G3; }
                    else if (current_char == '4') { current_state = State.G3; }
                    else if (current_char == '5') { current_state = State.G3; }
                    else if (current_char == '6') { current_state = State.G3; }
                    else if (current_char == '7') { current_state = State.G3; }
                    else if (current_char == '8') { current_state = State.G3; }
                    else if (current_char == '9') { current_state = State.G3; }
                    else { Stop = true; }
                    break;
                case State.G3:
                    if (Char.IsDigit(current_char)) { current_state = State.G3; }
                    else if (current_char == ')') { current_state = State.G4; }
                    else if (current_char == ' ') { current_state = State.G5; }
                    else { Stop = true; }
                    break;
                case State.G4:
                    if (current_char == ' ') { current_state = State.G4; }
                    else if (current_char == ',') { current_state = State.B; }
                    else if (current_char == ';') { current_state = State.F; }
                    else { Stop = true; }
                    break;
                case State.G5:
                    if (current_char == ' ') { current_state = State.G5; }
                    else if (current_char == ')') { current_state = State.G4; }
                    else { Stop = true; }
                    break;
                case State.D10:
                    if (current_char == ' ') { current_state = State.D10; }
                    else if (current_char == '(') { current_state = State.D3; }
                    else if (current_char == ',') { current_state = State.D1; }
                    else if (current_char == ')') { current_state = State.R; }
                    else { Stop = true; }
                    break;
                case State.CR:
                    if (current_char == ' ') { current_state = State.CR; }
                    else if (current_char == 'C') { current_state = State.Y1; }
                    else if (current_char == 'B') { current_state = State.R2; }
                    else if (current_char == 'F') { current_state = State.T1; }
                    else if (current_char == ',') { current_state = State.B; }
                    else if (current_char == ';') { current_state = State.F; }
                    else { Stop = true; }
                    break;
            }
        }


        if (Stop || ((pointer != line.Length - 1) && !Stop && (line[pointer] != ';')))
        {
            error = new Error() { Pointer = pointer, Comment = $"Состояние: {current_state}" };
        }

        if (Stop || ((pointer != line.Length - 1) && !Stop && (line[pointer] == ';')))
        {
            error = new Error() { Pointer = pointer, Comment = $"После ; ничего не проверяется!!!" };
        }
				else if (current_state != State.F)
        {
            error = new Error() { Pointer = pointer, Comment = $"Состояние: {current_state}" };
        }

        return new Report()
        {
            Error = error,
            StatesLog = StateLog,
            InputLine = line,
            Pointer = pointer
        };

    }
}


public struct Report
{
    public Error? Error { get; init; }
    public List<State> StatesLog { get; init; }
    public int Pointer { get; init; }
    public string InputLine { get; init; }
    public bool IsSuccess() => Error == null;
}



public struct Error
{
    public int Pointer { get; init; }
    public string Comment { get; init; }
}


public enum State
{
    S, A, A1, A2, B1, B2, B3, B4, B5, B6, B, C1, C2, C3, C13, C5, C, C4, C6, C15, C7, D1, D2, D3, D4, D5, D15, D6, D9, D7, D13, D8, R, C10, Y1, R2, T1, R1, R3, R4, R5, R6, R7, R8, R9, T2, T3, T4, Y2, Y3, T, G1, G2, G3, G4, G5, F, D10, CR

}
