﻿
using ReactiveUI;
using System;
using Core;
namespace WinApp.ViewModels;

public class MainWindowViewModel : ViewModelBase
{

    private string _ErrorReport;
    public string ErrorReport
    {
        get => _ErrorReport;

        set => this.RaiseAndSetIfChanged(ref _ErrorReport, value);

    }

    private string _InputLine;
    public string InputLine
    {
        get => _InputLine;

        set => this.RaiseAndSetIfChanged(ref _InputLine, value);
    }

    private string _NumbersList;

    public string NumbersList
    {
        get => _NumbersList;

        set => this.RaiseAndSetIfChanged(ref _NumbersList, value);
    }

    private string _IDList;

    public string IDList
    {
        get => _IDList;

        set => this.RaiseAndSetIfChanged(ref _IDList, value);
    }


    public MainWindowViewModel()
    {
    }

    public void CheckButton()
    {
				Console.WriteLine("Начинаем проверку");
        Report report = Core.Parser.GetReport(InputLine);
				Console.WriteLine($"Debug GUI Pointer = {report.Pointer}");
				var gui_report = UI.GetGuiReport(report);
				Console.WriteLine("Заканчиваем проверку");
				ErrorReport = gui_report.ErrorReport;
				NumbersList = gui_report.NumbersReport;
				IDList = gui_report.IDReport;
				
    }

}
